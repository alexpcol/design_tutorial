//
//  ActivityPresenter.swift
//  DesignCodeApp
//
//  Created by Mario on 9/25/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class ActivityPresenter: NSObject {
    static func showActivityViewController(withActivityItems activityItems: [Any], andExcludedActivities excludedActivities: [UIActivity.ActivityType], inView view: UIViewController)
    {
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityController.excludedActivityTypes = excludedActivities
        view.present(activityController, animated: true, completion: nil)
    }
}
