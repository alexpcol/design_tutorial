//
//  ImageService.swift
//  DesignCodeApp
//
//  Created by Mario on 9/25/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class ImageService: NSObject {

    static let cache = NSCache<NSString, UIImage>()
    
    static func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if let  downloadedimage = downloadedImage  {
                cache.setObject(downloadedimage, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
        }
        dataTask.resume()
    }
    
    static func getImage(withURL urlString:String, completion: @escaping (_ image:UIImage?)->()) {
        let url = URL(string: urlString)
        if let image = cache.object(forKey: urlString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url!, completion: completion)
        }
    }
}
