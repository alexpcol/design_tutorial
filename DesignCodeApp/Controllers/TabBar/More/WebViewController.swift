//
//  WebViewController.swift
//  DesignCodeApp
//
//  Created by Mario on 9/21/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var webView: WKWebView!
    var urlString: String!
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configureWeb()
    }
    
    // MARK: - General methods
    func configureWeb()
    {
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        webView.load(request)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    deinit {
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress"
        {
            if webView.estimatedProgress == 1.0
            {
                navigationItem.title = webView.title
            }
            else
            {
                navigationItem.title = "Loading..."
            }
        }
    }
    
    // MARK: - Action methods
    @IBAction func doneButtonTapped(_ sender: Any)
    {
        dismiss(animated: true)
    }
    
    @IBAction func actionButtontapped(_ sender: Any)
    {
        let activityItems = [urlString] as! Array<String>
    
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityController.excludedActivityTypes = [.postToFacebook]
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func safariButtonTapped(_ sender: Any)
    {
        UIApplication.shared.open(URL(string: urlString)!)
    }
    
    
    @IBAction func reload(_ sender: Any)
    {
        webView.reload()
    }
    
    @IBAction func goBack(_ sender: Any)
    {
        webView.goBack()
    }
    
    @IBAction func goForward(_ sender: Any)
    {
        webView.goForward()
    }
    
    
    
}
