//
//  MoreViewController.swift
//  DesignCodeApp
//
//  Created by Mario on 9/21/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {

    // MARK: - Variables
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - General methods
    
    
    // MARK: - Action methods
    @IBAction func safariButtonTapped(_ sender: Any)
    {
        performSegue(withIdentifier: "More to Web", sender: "https://designcode.io")
    }
    @IBAction func communityButtonTapped(_ sender: Any)
    {
        performSegue(withIdentifier: "More to Web", sender: "https://spectrum.chat/design-code")
    }
    @IBAction func twitterHandleTapped(_ sender: Any)
    {
        performSegue(withIdentifier: "More to Web", sender: "https://twitter.com/AlexPcol")
    }
    
    // MARK: - Segue methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier, identifier == "More to Web" {
            let toNav = segue.destination as! UINavigationController
            let toVC = toNav.viewControllers.first as! WebViewController
            toVC.urlString = sender as? String
        }
    }
}
