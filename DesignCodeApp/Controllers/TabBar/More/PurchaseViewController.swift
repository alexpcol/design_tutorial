//
//  PurchaseViewController.swift
//  DesignCodeApp
//
//  Created by Mario on 9/24/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class PurchaseViewController: UIViewController {
    // MARK: - Variables
    @IBOutlet var panToClose: InteractionPanToClose!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        panToClose.setGestureRecognizer()
    }
    
    
    // MARK: - General methods
    
    

}
