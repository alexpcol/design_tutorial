//
//  TestimonialViewController.swift
//  DesignCodeApp
//
//  Created by Mario on 9/13/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class TestimonialViewController: UIViewController {
    // MARK: - Variables
    
    @IBOutlet weak var testimonialCollectionView: UICollectionView!
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK: - General Methods
    func configureUI()
    {
        testimonialCollectionView.delegate = self
        testimonialCollectionView.dataSource = self
    }

}

extension TestimonialViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return testimonials.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "testimonialCell", for: indexPath) as! TestimonialCollectionViewCell
        let testimonial = testimonials[indexPath.row]
        cell.textLabel.text = testimonial["text"]
        cell.nameLabel.text = testimonial["name"]
        cell.jobLabel.text = testimonial["job"]
        //cell.jobLabel.sizeToFit()
        cell.avatarImageView.image = UIImage(named: testimonial["avatar"]!)
        return cell
    }
    
    
}
