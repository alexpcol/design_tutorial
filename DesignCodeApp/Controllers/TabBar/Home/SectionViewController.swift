//
//  SectionViewController.swift
//  DesignCodeApp
//
//  Created by Mario on 9/11/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class SectionViewController: UIViewController {

    // MARK: - Variables
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var subheadVisualEffectView: UIVisualEffectView!
    @IBOutlet weak var closeVisualEffectView: UIVisualEffectView!
    
    var section: [String: String]!
    
    var sections: [[String: String]]!
    var indexPath: IndexPath!
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK: - Action Methods
    @IBAction func closeButtonAction(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - General Methods
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func configureUI()
    {
        titleLabel.text = section["title"]
        captionLabel.text = section["caption"]
        bodyLabel.text = section["body"]
        coverImageView.image = UIImage(named: section["image"]!)
        
        progressLabel.text = "\(indexPath.row+1) / \(sections.count)"
    }
}
