//
//  TestimonialCollectionViewCell.swift
//  DesignCodeApp
//
//  Created by Mario on 9/13/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class TestimonialCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
}
