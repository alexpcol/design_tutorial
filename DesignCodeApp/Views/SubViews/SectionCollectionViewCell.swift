//
//  SectionCollectionViewCell.swift
//  DesignCodeApp
//
//  Created by Mario on 9/11/18.
//  Copyright © 2018 Meng To. All rights reserved.
//

import UIKit

class SectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    
    
}
